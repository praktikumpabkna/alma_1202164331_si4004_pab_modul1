package com.example.android.studycase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private EditText a, t;
    private TextView hasil;
    private Button btnCek;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        a = (EditText) findViewById(R.id.txtalas);
        t = (EditText) findViewById(R.id.txttinggi);
        hasil = (TextView)findViewById(R.id.txthasil);
        btnCek = (Button) findViewById(R.id.btncek);

        btnCek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int alas = Integer.parseInt(a.getText().toString());
                int tinggi = Integer.parseInt(t.getText().toString());
                int hsl = alas * tinggi;

                hasil.setText (hsl + "");
            }
        });
    }
}